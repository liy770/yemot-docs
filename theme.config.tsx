import React from 'react'
import { DocsThemeConfig, useConfig } from 'nextra-theme-docs'

const config: DocsThemeConfig = {
  direction: 'rtl',
  logo: <span>ימות המשיח - תיעוד הגדרות</span>,
  docsRepositoryBase: 'https://gitlab.com/liy770/yemot-docs/-/tree/main/',
  footer: {
    text: 'שוכתב ע״י @liy',
  },
  search: {
    placeholder: 'חיפוש בתיעוד',
  },
  toc: {
    title: 'תוכן עניינים'
  },
  feedback: {
    content: 'משוב על העמוד'
  },
  editLink: {
    text: 'ערוך את העמוד'
  },
  useNextSeoProps() {
    const { frontMatter } = useConfig()
    return {
      titleTemplate: '%s – ימות המשיח - תיעוד הגדרות',
      description:
        frontMatter.description || 'תיעוד הגדרות של מערכות ימות המשיח',
    }
  }
}

export default config
